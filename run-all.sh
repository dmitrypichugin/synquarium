#!/bin/sh

cd synquarium
mvn clean package
cd ..

export RUN_API="cd `pwd`; java -jar synquarium/target/synquarium-0.0.1-SNAPSHOT.jar --mode=api"
export RUN_AGENT="cd `pwd`; java -jar synquarium/target/synquarium-0.0.1-SNAPSHOT.jar --mode=agent"

osascript -e 'tell app "Terminal" to do script (system attribute "RUN_API")'
osascript -e 'tell app "Terminal" to do script (system attribute "RUN_AGENT")'
