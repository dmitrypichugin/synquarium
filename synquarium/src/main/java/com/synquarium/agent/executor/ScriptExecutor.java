package com.synquarium.agent.executor;

public interface ScriptExecutor {
	
	ScriptExecutorResult execute(String script);
	
}
