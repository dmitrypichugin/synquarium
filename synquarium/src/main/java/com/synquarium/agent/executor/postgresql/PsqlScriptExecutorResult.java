package com.synquarium.agent.executor.postgresql;

import com.synquarium.agent.SyncStatusEnum;
import com.synquarium.agent.executor.ScriptExecutorResult;

public class PsqlScriptExecutorResult implements ScriptExecutorResult {

	private SyncStatusEnum status;
	private String message;
	
	@Override
	public SyncStatusEnum getStatus() {
		return status;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setStatus(SyncStatusEnum status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
