package com.synquarium.agent.executor;

import com.synquarium.agent.SyncStatusEnum;

public interface ScriptExecutorResult {

	SyncStatusEnum getStatus();

	String getMessage();
	
}
