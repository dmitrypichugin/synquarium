package com.synquarium.agent.executor.postgresql;

import com.synquarium.agent.SyncStatusEnum;
import com.synquarium.agent.executor.ScriptExecutor;
import com.synquarium.agent.executor.ScriptExecutorResult;

public class PsqlScriptExecutor implements ScriptExecutor {

	@Override
	public ScriptExecutorResult execute(String script) {
		System.out.println("EXEC script: " + script);
		
		//TODO: Execute the script, parse the result and return the corresponding object
		PsqlScriptExecutorResult result = new PsqlScriptExecutorResult();
//		result.setMessage("Script failed due to: ...");
		result.setStatus(SyncStatusEnum.OK);
		
		return result;
	}

}
