package com.synquarium.agent;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.synquarium.agent.domain.instance.InstanceInfoRepository;
import com.synquarium.common.domain.instance.InstanceInfo;
import com.synquarium.common.domain.instance.InstanceInfo.SyncScheduleEnum;

@Service
@Profile("agent")
public class InstanceInfoWatcher {

	@Autowired
	private InstanceInfoRepository instanceInfoRepo;
	@Autowired
	private SynchronizationService synchronizationService;
	
	public void startWatching() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			private boolean syncInProgress;
			
			@Override public void run() {
				if (syncInProgress) {
					return;
				}
				syncInProgress = true;
				InstanceInfo instanceInfo = getInstanceInfo();
				System.out.println("Checking auto-synchronization: " + instanceInfo.getSyncSchedule());
				if (instanceInfo.getSyncSchedule() == SyncScheduleEnum.AUTO) {
					synchronizationService.synchronize(instanceInfo);
				}
				syncInProgress = false;
			}
		}, 0, 5000);
		//TODO: Run synchronization in a separate timer and make the delay configurable, 
		//      while instance info watcher's timeout can still be static.
	}
	
	private InstanceInfo getInstanceInfo() {
		return instanceInfoRepo.findAll().iterator().next();
	}
}
