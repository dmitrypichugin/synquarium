package com.synquarium.agent;

import com.synquarium.common.domain.instance.InstanceInfo;

public interface SynchronizationService {

	void synchronize(InstanceInfo instanceInfo);

}
