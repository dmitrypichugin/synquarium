package com.synquarium.agent.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import com.synquarium.agent.executor.ScriptExecutor;

@Configuration
@Profile("agent")
public class AgentAppConfig {
	
	@Autowired
	private Environment env;
	
	@Bean
	public ScriptExecutor scriptExecutor() {
		String executorClassName = env.getProperty("scriptExecutor.classname");
		try {
			return (ScriptExecutor) Class.forName(executorClassName).newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Executor class not found: " + executorClassName);
		}
	}
	
}
