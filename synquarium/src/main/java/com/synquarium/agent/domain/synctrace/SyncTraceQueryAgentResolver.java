package com.synquarium.agent.domain.synctrace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.synquarium.common.domain.synctrace.SyncTrace;

@Component
@Profile("agent")
public class SyncTraceQueryAgentResolver implements GraphQLQueryResolver {

	@Autowired
	private SyncTraceRepository syncRepo;
	
	public Iterable<SyncTrace> findSyncs(Long releaseId, Number page, Number pageSize) {
		return syncRepo.findByReleaseIdOrderById(releaseId, PageRequest.of(page.intValue(), pageSize.intValue()));
	}
	
}
