package com.synquarium.agent.domain.instance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.synquarium.common.domain.instance.InstanceInfo;

@Component
@Profile("agent")
public class InstanceInfoResolver implements GraphQLQueryResolver {

	@Autowired
	private InstanceInfoRepository instanceInfoRepo;
	
	public InstanceInfo getInstanceInfo() {
		return instanceInfoRepo.findAll().iterator().next();
	}
	
}
