package com.synquarium.agent.domain.instance;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.synquarium.common.domain.instance.InstanceInfo;

public interface InstanceInfoRepository extends PagingAndSortingRepository<InstanceInfo, Long> {

	
}
