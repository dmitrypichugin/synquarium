package com.synquarium.agent.domain.instance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.synquarium.common.domain.instance.InstanceInfo;

@Component
@Profile("agent")
public class InstanceMutationAgentResolver implements GraphQLMutationResolver {

	@Autowired
	private InstanceInfoRepository instanceInfoRepo;
	
	public InstanceInfo updateInstanceInfo(InstanceInfo instanceInfo) throws JsonProcessingException {
		InstanceInfo old = instanceInfoRepo.findAll().iterator().next(); //instanceInfoRepo.findOne(instanceInfo.getId());
		boolean requireUpdate = false;
		if (notEqual(instanceInfo.getReleaseId(), old.getReleaseId())) {
			old.setReleaseId(instanceInfo.getReleaseId());
			requireUpdate = true;
		}
		if (notEqual(instanceInfo.getSyncSchedule(), old.getSyncSchedule())) {
			old.setSyncSchedule(instanceInfo.getSyncSchedule());
			requireUpdate = true;
		}
		if (notEqual(instanceInfo.getReleaseIdToStartWith(), old.getReleaseIdToStartWith())) {
			old.setReleaseIdToStartWith(instanceInfo.getReleaseIdToStartWith());
			requireUpdate = true;
		}
		if (requireUpdate) {
			return instanceInfoRepo.save(old);
		}
		return old;
	}
	
	private boolean notEqual(Object o1, Object o2) {
		return o1 != null && !o1.equals(o2);
	}
	
}
