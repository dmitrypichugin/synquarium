package com.synquarium.agent.domain.synctrace;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.synquarium.common.domain.synctrace.SyncTrace;


public interface SyncTraceRepository extends PagingAndSortingRepository<SyncTrace, Long> {

	Page<SyncTrace> findByReleaseIdOrderById(Long releaseId, Pageable page);
	
//	List<SyncTrace> findByIdIn(List<Long> ids);

}
