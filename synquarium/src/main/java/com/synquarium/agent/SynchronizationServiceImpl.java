package com.synquarium.agent;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.synquarium.agent.domain.instance.InstanceInfoRepository;
import com.synquarium.agent.domain.synctrace.SyncTraceRepository;
import com.synquarium.agent.executor.ScriptExecutor;
import com.synquarium.agent.executor.ScriptExecutorResult;
import com.synquarium.common.domain.instance.InstanceInfo;
import com.synquarium.common.domain.synctrace.SyncTrace;
import com.synquarium.http.GraphqlHttpClient;

@Service
@Profile("agent")
public class SynchronizationServiceImpl implements SynchronizationService {

	@Autowired
	private GraphqlHttpClient graphqlClient;
	@Autowired
	private SyncTraceRepository syncTraceRepo;
	@Autowired
	private Environment env;
	@Autowired
	private ScriptExecutor scriptExecutor;
	@Autowired
	private InstanceInfoRepository instanceInfoRepo;
	
	@Override
	public void synchronize(InstanceInfo instanceInfo) {
		
		String params = "lastSynced: " + instanceInfo.getLastSynced().getTime() + 
				", upToReleaseId: " + instanceInfo.getReleaseId();
		if (instanceInfo.getReleaseIdToStartWith() != null) {
			params += ", fromReleaseId: " + instanceInfo.getReleaseIdToStartWith();
		}
		String fieldsToReturn = "id, releaseId, hash, body";
		SyncTrace[] syncTraces = graphqlClient.post(
				env.getProperty("repository.url", "http://localhost:8080") + "/graphql",
				"findNewSyncs", SyncTrace[].class,
				"{\"query\":\"{findNewSyncs(" + params + ") { " + fieldsToReturn + " }}\"}");
		
		System.out.println("Syncs received: " + syncTraces.length);
		
		//TODO: Review this code. Here we rely on the releaseId order, which is not necesserally the correct order:
		Map<Long, List<SyncTrace>> syncTracesByRelease = 
				Arrays.stream(syncTraces).collect(Collectors.groupingBy(SyncTrace::getReleaseId));

		// For each release that has changes try to find 
		for (Map.Entry<Long, List<SyncTrace>> entry : syncTracesByRelease.entrySet()) {
			if (syncTraceRepo.findAllById(entry.getValue().stream().map(sync -> sync.getId()).collect(Collectors.toList()))
					.iterator().hasNext()) {
				//TODO: set the database status to "REQUIRED RESTORE or something like this and STOP HERE!
				System.out.println("DB requires RESTORE");
//				return;
			}
		}
		
		// Apply the scripts (body) in the order they were returned:
		for (SyncTrace syncTrace : syncTraces) {
			ScriptExecutorResult result = scriptExecutor.execute(syncTrace.getBody());
			syncTrace.setSyncStatus(result.getStatus());
			syncTrace.setSyncMessage(result.getMessage());
			syncTraceRepo.save(syncTrace);
		}
		
		// Update the last synchronization timestamp:
		InstanceInfo instanceInfoFromDb = instanceInfoRepo.findById(instanceInfo.getId()).get();
		instanceInfoFromDb.setLastSynced(new Date());
		instanceInfoRepo.save(instanceInfoFromDb);
		
		//We don't need this any longer:
//		Long[] releases = syncTracesByRelease.keySet().toArray(new Long[syncTracesByRelease.size()]);
//		Arrays.sort(releases);
		
		
	}

}
