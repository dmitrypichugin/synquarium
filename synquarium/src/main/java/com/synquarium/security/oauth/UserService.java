package com.synquarium.security.oauth;

import java.util.List;

public interface UserService {

//	User loadUserByUsername(String email) throws UsernameNotFoundException;
	
	List<User> findAll();
	
	User save(User user);

	void delete(Long id);
	
}
