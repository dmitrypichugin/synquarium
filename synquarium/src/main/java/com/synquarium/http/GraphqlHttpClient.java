package com.synquarium.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

//TODO: extract this class into a separate library since it's being using in both Agent and API modules!
@Service
public class GraphqlHttpClient {

	public <T> T post(String url, String methodName, Class<T> resultClass, String jsonBody) {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		try {
			StringEntity entity = new StringEntity(jsonBody);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
	
			CloseableHttpResponse response = client.execute(httpPost);
			try (BufferedReader buffer = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
				String responseJson = buffer.lines().collect(Collectors.joining("\n"));

				System.out.println(responseJson);

				ObjectMapper objectMapper = new ObjectMapper();
				JsonNode node = objectMapper.readTree(responseJson);
				return objectMapper.convertValue(node.get("data").get(methodName), resultClass);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
