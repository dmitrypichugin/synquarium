package com.synquarium.api.domain.sync;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;

import graphql.Assert;

@Component
@Profile("api")
public class SyncMutationResolver implements GraphQLMutationResolver /*GraphQLRootResolver */{

	@Autowired
	private SyncRepository syncRepo;
	@Autowired
	private SyncTargetRepository syncTargetRepo;
	@Autowired
	private SyncMergeService syncMergeService;

	public Sync createSync(String name, String body, /*Long authorId,*/ /*String hash,*/
			/*Boolean approved,*/ Long releaseId, Long position, List<Long> targetIds) {
		Sync sync = syncRepo.save(new Sync(name, body, 1L, String.valueOf(body.hashCode()), 
				false, releaseId, adjustPosition(position, releaseId)));
		if (targetIds != null) {
			targetIds.stream().forEach(t -> syncTargetRepo.save(new SyncTarget(sync.getId(), t)));
		}
		return sync;
	}
	
	private Long adjustPosition(Long position, Long releaseId) {
		if (position == null && releaseId != null) {
			position = syncRepo.getMaxSyncPosition(releaseId);
		}
		return position == null ? 1 : position;
	}

	public Sync updateSync(Long id, Sync sync, List<Long> targetIds) {
		Assert.assertNotNull(id, "Sync ID is required for the update operation.");

		Sync updatedSync = sync != null ? updateSync(id, sync) : syncRepo.findById(id).get();

		if (targetIds != null) {
			updateSyncTargets(id, targetIds);
		}
		return updatedSync;
	}
	
	//TODO: Perhaps we can expose this method as public:
	private Sync updateSync(Long id, Sync sync) {
		Assert.assertNotNull(id, "Sync ID is required for the update operation.");
		Sync orig = syncRepo.findById(id).get();
		Assert.assertNotNull(orig, "Sync not found, ID: " + id);

		//TODO: Use Apache's bean library to copy objects:
		sync.setId(id);
		sync.setAuthorId(orig.getAuthorId());
		sync.setCreated(orig.getCreated());
		if (sync.getApproved() == null) {
			sync.setApproved(orig.getApproved());
		}
		if (sync.getName() == null) {
			sync.setName(orig.getName());
		}
		if (sync.getBody() == null) {
			sync.setBody(orig.getBody());
			sync.setHash(orig.getHash());
		} else {
			sync.setHash(String.valueOf(sync.getBody().hashCode()));
		}
		if (sync.getPosition() == null) {
			sync.setPosition(orig.getPosition());
		}
		if (sync.getReleaseId() == null) {
			sync.setReleaseId(orig.getReleaseId());
		}
		
		// For now let's assume we change the position by sending the new position inside the object,
		// later we might want to consider sending the id of the new neighbour instead:
		if (sync.getPosition() == null) {
			sync.setPosition(orig.getPosition());
		} else if (sync.getPosition() != orig.getPosition()) {
			incrementPositionsOfSyncsThatFollow(sync);
		}
		if (isSyncRequired(sync, orig)) {
			sync.setRequiresSyncSince(new Date());
		}
		return syncRepo.save(sync);
	}
	
	private boolean isSyncRequired(Sync sync, Sync orig) {
		return !sync.getBody().equals(orig.getBody())
				|| !sync.getPosition().equals(orig.getPosition())
				|| !sync.getReleaseId().equals(orig.getReleaseId());
	}
	
	private void incrementPositionsOfSyncsThatFollow(Sync sync) {
		Date now = new Date();
		syncRepo.findByReleaseIdAndPositionGreaterThanEqualOrderByPosition(
				sync.getReleaseId(), sync.getPosition()).forEach(followingSync -> {
					followingSync.setPosition(followingSync.getPosition() + 1);
					followingSync.setRequiresSyncSince(now);
					syncRepo.save(followingSync);
				});
	}
	
	public void updateSyncTargets(Long id, List<Long> targetIds) {
		List<SyncTarget> origTargets = syncTargetRepo.findBySyncId(id);
		List<SyncTarget> targetsToDelete = origTargets.stream()
				.filter(t -> !targetIds.contains(t.getTargetId()))
				.collect(Collectors.toList());
		syncTargetRepo.deleteAll(targetsToDelete);
		
		List<Long> origTargetIds = origTargets.stream().map(t -> t.getTargetId()).collect(Collectors.toList());
		List<SyncTarget> targetsToAdd = targetIds.stream()
				.filter(p -> !origTargetIds.contains(p))
				.map(targetId -> new SyncTarget(id, targetId))
				.collect(Collectors.toList());
		targetsToAdd.stream().forEach(t -> syncTargetRepo.save(t));
	}
	
	public Long deleteSync(Long id) {
		syncRepo.deleteById(id);
		return id;
	}
	
	public List<Sync> mergeSyncs(Long fromReleaseId, Long toReleaseId) {
		return syncMergeService.merge(fromReleaseId, toReleaseId);
	}
	
}
