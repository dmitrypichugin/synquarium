package com.synquarium.api.domain.instance;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
@Profile("api")
public interface InstanceRepository extends CrudRepository<Instance, Long> {

}
