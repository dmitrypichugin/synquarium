package com.synquarium.api.domain.release;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
@Profile("api")
public class ReleaseResolver implements GraphQLQueryResolver {

	@Autowired
	private ReleaseRepository releaseRepo;
	
	public Iterable<Release> getAllReleases() {
		return releaseRepo.findAll();
	}
	
}
