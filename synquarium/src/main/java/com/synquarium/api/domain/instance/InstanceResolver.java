package com.synquarium.api.domain.instance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.synquarium.common.domain.instance.InstanceInfo;
import com.synquarium.http.GraphqlHttpClient;

import graphql.Assert;

@Component
@Profile("api")
public class InstanceResolver implements GraphQLQueryResolver {

	@Autowired
	private InstanceRepository instanceRepo;
	@Autowired
	private GraphqlHttpClient graphqlClient;

	public Iterable<Instance> getInstances() {
		return instanceRepo.findAll();
	}
	
	public InstanceInfo getInstanceInfo(Long instanceId) {
		Instance instance = instanceRepo.findById(instanceId).get();
		Assert.assertNotNull(instance, "Instance with id:" + instanceId + " not found.");

		InstanceInfo instanceInfo = graphqlClient.post(
				instance.getUrl() + "/graphql", "getInstanceInfo", InstanceInfo.class,
				"{\"query\":\"{getInstanceInfo { id, releaseId, lastSynced, syncSchedule, fullRestoreRequired }}\"}");

		//We don't need the instanceInfo.id from the agent, it only has one id... we need the API server's instance id:
		instanceInfo.setId(instanceId);
		return instanceInfo;
	}
	
}
