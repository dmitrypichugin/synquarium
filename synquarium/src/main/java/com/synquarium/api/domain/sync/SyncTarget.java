package com.synquarium.api.domain.sync;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(SyncTarget.class)
public class SyncTarget implements Serializable {

	private @Id Long syncId;
	private @Id Long targetId;

	public SyncTarget() {
	}

	public SyncTarget(Long syncId, Long targetId) {
		this.syncId = syncId;
		this.targetId = targetId;
	}

	public Long getSyncId() {
		return syncId;
	}

	public void setSyncId(Long syncId) {
		this.syncId = syncId;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

}
