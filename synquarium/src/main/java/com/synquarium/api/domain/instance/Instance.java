package com.synquarium.api.domain.instance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This class contains meta information about the target environment that needs to be
 * synced, e.g. a copy of a customer's database running on a QA server. Let's
 * say we have three customers: A, B and C. Each customer has their own
 * environment where they run our application and database intance. Our QA team
 * wants to run a copy of each customer's database on the QA server. In this
 * case we'll have three "instances" that Synquarium server needs to be pointed
 * at for it to be able to synchronize all three of them whenever the DB schema
 * changes.
 *
 */
@Entity
public class Instance {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String url;
//	private String username;
//	private String password;
//	private Long releaseId;

	public Instance() {}
	public Instance(String name, String url) {
		this(null, name, url);
	}
	public Instance(Long id, String name, String url) {
		this.id = id;
		this.name = name;
		this.url = url;
//		this.username = username;
//		this.password = password;
//		this.releaseId = releaseId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
