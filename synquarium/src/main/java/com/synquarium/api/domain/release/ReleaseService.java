package com.synquarium.api.domain.release;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReleaseService {
	@Autowired private ReleaseRepository releaseRepo;
	
	public boolean isParentOfChild(Long parentReleaseId, Long childReleaseId) {
		List<Release> releasePath = this.getReleasePath(parentReleaseId, childReleaseId);
		return releasePath.size() > 1 && releasePath.get(0).getId().equals(parentReleaseId);
	}
	
	public List<Release> getReleasePath(Long fromReleaseId, Long toReleaseId) {
		List<Release> releasePath = new ArrayList<Release>();
		Map<Long, Release> releaseMap = new HashMap<Long, Release>();
		releaseRepo.findAll().forEach(release -> releaseMap.put(release.getId(), release));
		Release release = releaseMap.get(toReleaseId);
		while (release != null && (fromReleaseId == null || fromReleaseId.equals(release.getParentId()))) {
			releasePath.add(release);
			release = releaseMap.get(release.getParentId());
		}
		Collections.reverse(releasePath);
		return releasePath;
	}
	
}
