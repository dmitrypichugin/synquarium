package com.synquarium.api.domain.instance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synquarium.api.domain.release.ReleaseService;
import com.synquarium.common.domain.instance.InstanceInfo;
import com.synquarium.http.GraphqlHttpClient;

import graphql.Assert;

@Component
@Profile("api")
public class InstanceMutationApiResolver implements GraphQLMutationResolver {

	@Autowired private InstanceRepository instanceRepo;
	@Autowired private GraphqlHttpClient graphqlClient;
	@Autowired private InstanceResolver instanceResolver;
	@Autowired private ReleaseService releaseService;
	
	public Instance createInstance(Instance instance/*String name, String url, String username, String password, Long releaseId*/) {
		return instanceRepo.save(instance);
	}
	
	public Instance updateInstance(Instance instance) {
		return instanceRepo.save(instance);
	}
	
	public Long deleteInstance(Long id) {
		instanceRepo.deleteById(id);
		return id;
	}

	public InstanceInfo updateInstanceInfo(InstanceInfo instanceInfo) throws JsonProcessingException {
		Instance instance = instanceRepo.findById(instanceInfo.getId()).get();
		Assert.assertNotNull(instance, "Instance with id:" + instanceInfo.getId() + " not found.");

		if (isReleaseChangedAndInstanceRequiresRestore(instanceInfo.getId(), instanceInfo.getReleaseId())) {
			instanceInfo.setFullRestoreRequired(Boolean.TRUE);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		objectMapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);
		String instanceInfoJson = objectMapper.writeValueAsString(instanceInfo)
				.replaceAll("\\{\\\"", "{")
				.replaceAll(",\\\"", ",")
				.replaceAll("\\\":", ":")
				.replaceAll("\"", "\\\\\"");

		System.out.println("Updating instance: " + instance.getUrl() + "... " + instance);
		InstanceInfo updatedInstanceInfo = graphqlClient.post(
				instance.getUrl() + "/graphql", "updateInstanceInfo", InstanceInfo.class,
				"{\"operationName\":\"updateInstanceInfo\","
				+ "\"variables\":null," //{\"instanceInfo\":{\"id\":\"1\",\"releaseId\":3,\"syncSchedule\":\"AUTO\"},"
				+ "\"query\":\"mutation updateInstanceInfo {\\n" + 
//				"  updateInstanceInfo(instanceInfo: {id:1, releaseId:1, syncSchedule:\\\"AUTO\\\" }) {\\n" + 
				"  updateInstanceInfo(instanceInfo: " + instanceInfoJson + ") {\\n" + 
				"    id,\\n" + 
				"    releaseId,\\n" + 
				"    lastSynced,\\n" + 
				"    syncSchedule,\\n" + 
				"    fullRestoreRequired\\n" + 
				"  }\\n" + 
				"}\"}");
		return updatedInstanceInfo;
	}
	
	private boolean isReleaseChangedAndInstanceRequiresRestore(Long instanceId, Long newReleaseId) {
		InstanceInfo instanceInfo = instanceResolver.getInstanceInfo(instanceId);
		Long oldReleaseId = instanceInfo.getReleaseId();
		if (newReleaseId != null && oldReleaseId != null && !newReleaseId.equals(oldReleaseId)) {
			return !releaseService.isParentOfChild(oldReleaseId, newReleaseId);
		}
		return false;
	}
	
}
