package com.synquarium.api.domain.sync;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;

@Component
@Profile("api")
public class SyncResolver implements GraphQLResolver<Sync> {

	@Autowired
	private SyncTargetRepository syncTargetRepo;
	
	public List<Long> getTargets(Sync sync) {
		return syncTargetRepo.findBySyncId(sync.getId())
				.stream()
				.map(syncTarget -> syncTarget.getTargetId())
				.collect(Collectors.toList());
	}
	
}
