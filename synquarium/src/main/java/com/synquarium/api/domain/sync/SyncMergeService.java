package com.synquarium.api.domain.sync;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synquarium.api.domain.release.ReleaseService;

@Service
public class SyncMergeService {

	@Autowired private ReleaseService releaseService;
	@Autowired private SyncRepository syncRepo;

	
	/* 
	 * Merge process:
	 *  1. Select the release branch to merge from
	 *  2. Determine the common parent if any between the selected and the current releases
	 *  3. Get all releases between the common parent and the selected release
	 *  4. Get all syncs for the found releases
	 *  5. Copy all the found syncs excluding the ones that have already been merged
	 *     (use hash code to tell whether the sync has already been merged)
	 * */
	@Transactional
	public List<Sync> merge(Long releaseA, Long releaseB) {
		// Find releases that need to be merged:
		List<Long> releaseAPath = releaseService.getReleasePath(null, releaseA).stream().map(release -> release.getId()).collect(Collectors.toList());
		List<Long> releaseBPath = releaseService.getReleasePath(null, releaseB).stream().map(release -> release.getId()).collect(Collectors.toList());
		List<Long> releasesToBeMerged = new ArrayList<Long>();
		for (int i = releaseAPath.size() - 1; i >= 0; i--) { // Reversing through the list
			Long id = releaseAPath.get(i);
			if (releaseBPath.contains(id)) {
				break; // Stop once the common parent is found
			}
			releasesToBeMerged.add(id);
		}
		
		// Get syncs for the releases:
		List<Sync> conflictingSyncs = new ArrayList<Sync>();
		for (Long releaseId : releasesToBeMerged) {
			List<Sync> syncs = syncRepo.findByReleaseIdOrderByPosition(releaseId);
			for (Sync sync : syncs) {
				if (syncRepo.existsByHashAndReleaseId(sync.getHash(), releaseB)) {
					conflictingSyncs.add(sync);
				} else {
					sync = createCopyForRelease(sync, releaseB);
					Long position = syncRepo.getMaxSyncPosition(releaseB);
					sync.setPosition(position == null ? 1 : position + 1);
					syncRepo.save(sync);
				}
			}
		}
		return conflictingSyncs; // Returns the list of syncs that have already been merged
	}
	
	private Sync createCopyForRelease(Sync o, Long releaseId) {
		Sync copy = new Sync();
		// ID is set to null since we're are creating a new sync
		// Position is determined dynamically in the INSERT statement
		copy.setReleaseId(releaseId);
		copy.setApproved(o.getApproved());
		copy.setAuthorId(o.getAuthorId());
		copy.setBody(o.getBody());
		copy.setCreated(o.getCreated());
		copy.setHash(o.getHash());
		copy.setName(o.getName());
		Date now = new Date();
		copy.setRequiresSyncSince(now);
		copy.setUpdated(now);
		return copy;
	}

}
