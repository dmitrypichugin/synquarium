package com.synquarium.api.domain.release;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Release {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private @NotNull String name;
	private Long parentId; // we can have a release tree
	
	public Release() {
	}
	
	public Release(String name) {
		this.name = name;
	}

	public Release(String name, Long parentId) {
		this.name = name;
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "Release [id=" + id + ", name=" + name + ", parentId=" + parentId + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
}
