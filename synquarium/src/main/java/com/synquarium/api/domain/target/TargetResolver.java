package com.synquarium.api.domain.target;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
@Profile("api")
public class TargetResolver implements GraphQLQueryResolver {

	@Autowired
	private TargetRepository targetRepo;
	
	public Iterable<Target> getAllTargets() {
		return targetRepo.findAll();
	}
	
}
