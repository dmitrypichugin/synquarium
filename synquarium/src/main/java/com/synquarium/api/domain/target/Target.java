package com.synquarium.api.domain.target;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

/**
 * This class represents types of clients (or target environments).
 * */
@Entity
public class Target {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private @NotNull String name;
	private @CreatedDate Date created;
	private @UpdateTimestamp Date updated;
	
	public Target() {}
	public Target(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Target [id=" + id + ", name=" + name + ", created=" + created + ", updated=" + updated + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
