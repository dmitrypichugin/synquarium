package com.synquarium.api.domain.sync;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SyncRepository extends PagingAndSortingRepository<Sync, Long> {

	Page<Sync> findByReleaseIdOrderByPosition(Long releaseId, Pageable page);
	List<Sync> findByReleaseIdOrderByPosition(Long releaseId);
	
	List<Sync> findByReleaseIdAndPositionGreaterThanEqualOrderByPosition(Long releaseId, Long position);

	@Query(value = "SELECT * FROM sync WHERE requires_Sync_Since >= :lastSynced AND release_id = :releaseId ORDER BY position",
			nativeQuery = true)
	List<Sync> findByRequiresSyncSinceAndRelease(
			@Param("lastSynced") Date lastSynced,
			@Param("releaseId") Long releaseId);

	//========================================================================================
	// The following query builds up a tree branch recursively using the parentId
	// It is an extremely ugly and confusing piece of SQL. Let's just leave it here for future
	// reference only:
	//
	//	@Query(value = "WITH link(id, name, pid, rank) AS ("
	//			+ " SELECT id, name, parent_id, 0 FROM release WHERE id = ?2"
	//			+ " UNION ALL"
	//			+ " SELECT release.id, release.name, parent_id, rank+1"
	//			+ " FROM link INNER JOIN release ON link.pid = release.id"
	//			+ " ) "
	//			+ " SELECT link.id, link.name, link.rank, s.*"
	//			+ " FROM link"
	//			+ " INNER JOIN sync s ON s.release_id = link.id"
	//			+ " WHERE s.requires_Sync_Since >= ?1"
	//			+ " ORDER BY link.rank DESC, s.position",
	//		nativeQuery = true)
	//	List<Sync> findByRequiresSyncSinceAndUpToRelease(Date lastSynced, Long upToReleaseId);
	//========================================================================================

	@Transactional(readOnly = true)
	@Query(value = "SELECT max(position) FROM sync WHERE release_id = :releaseId", 
			nativeQuery = true)
	Long getMaxSyncPosition(@Param("releaseId") Long releaseId);
	
	@Transactional(readOnly = true)
	boolean existsByHashAndReleaseId(String hash, Long releaseId);

}
