package com.synquarium.api.domain.sync;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class Sync {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private @NotNull String name;
	private @NotNull String body;
	private @CreationTimestamp Date created;
	private @UpdateTimestamp Date updated;
	private @CreationTimestamp Date requiresSyncSince; // This date gets updated to the current time when the sync changes,
	                                                   // consider replacing it with the "updated" field. 
	private @NotNull Long authorId;
	private String hash; // Hash code of the body field
	private Boolean approved;
	private @NotNull Long releaseId;
	private @NotNull Long position; //An item is always moved within a release, so there
	                                //is always a finite number of items. Once we move
	                                //an item we update all items between the old and new positions
	                                //re-indexing them. Stupid, but simple and should suffice for now.
	
	public Sync() {}
	
	public Sync(String name, String body, Long authorId, String hash,
			Boolean approved, Long releaseId, Long position) {
		this.name = name;
		this.body = body;
		this.authorId = authorId;
		this.hash = hash;
		this.approved = approved;
		this.releaseId = releaseId;
		this.position = position;
	}
	
	@Override
	public String toString() {
		return "Sync [id=" + id + ", name=" + name + ", body=" + body + ", created=" + created + ", updated=" + updated
				+ ", requiresSyncSince=" + requiresSyncSince + ", authorId=" + authorId + ", hash=" + hash
				+ ", approved=" + approved + ", releaseId=" + releaseId + ", position=" + position + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public Long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}

	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	public Date getRequiresSyncSince() {
		return requiresSyncSince;
	}

	public void setRequiresSyncSince(Date requiresSyncSince) {
		this.requiresSyncSince = requiresSyncSince;
	}

}
