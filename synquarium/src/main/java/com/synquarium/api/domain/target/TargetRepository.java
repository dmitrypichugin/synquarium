package com.synquarium.api.domain.target;

import org.springframework.data.repository.CrudRepository;

public interface TargetRepository extends CrudRepository<Target, Long> {

//	List<Target> findBySyncId(Long syncId);
	
}
