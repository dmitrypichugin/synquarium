package com.synquarium.api.domain.sync;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.synquarium.api.domain.release.Release;
import com.synquarium.api.domain.release.ReleaseService;

@Component
@Profile("api")
public class SyncQueryResolver implements GraphQLQueryResolver {

	@Autowired private SyncRepository syncRepo;
	@Autowired private ReleaseService releaseService;
	
	public Iterable<Sync> findSyncs(Long releaseId, Number page, Number pageSize) {
		return syncRepo.findByReleaseIdOrderByPosition(releaseId, PageRequest.of(page.intValue(), pageSize.intValue()));
	}

	public Iterable<Sync> findNewSyncs(Date lastSynced, Long toReleaseId, Long fromReleaseId /*optional*/) {
		List<Release> releases = releaseService.getReleasePath(fromReleaseId, toReleaseId);
		List<Sync> syncs = new ArrayList<Sync>();
		releases.forEach(r -> syncs.addAll(syncRepo.findByRequiresSyncSinceAndRelease(lastSynced, r.getId())));
		return syncs;
	}

	public Sync getSync(Long id) {
		return syncRepo.findById(id).get();
	}
	
}
