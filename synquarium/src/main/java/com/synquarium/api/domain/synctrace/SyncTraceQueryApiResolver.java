package com.synquarium.api.domain.synctrace;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.synquarium.api.domain.instance.Instance;
import com.synquarium.api.domain.instance.InstanceRepository;
import com.synquarium.common.domain.synctrace.SyncTrace;
import com.synquarium.http.GraphqlHttpClient;

import graphql.Assert;

@Component
@Profile("api")
public class SyncTraceQueryApiResolver implements GraphQLQueryResolver {

	@Autowired
	private InstanceRepository instanceRepo;
	@Autowired
	private GraphqlHttpClient graphqlClient;
	
	public Iterable<SyncTrace> getInstanceSyncs(Long instanceId, Long releaseId) {
		Instance instance = instanceRepo.findById(instanceId).get();
		Assert.assertNotNull(instance, "Instance with id:" + instanceId + " not found.");
		
		return Arrays.asList(graphqlClient.post(instance.getUrl() + "/graphql", "findSyncs", SyncTrace[].class,
				"{\"query\":\"{ findSyncs (releaseId: " + releaseId + 
				") { id, created, lastSynced, hash, syncStatus, syncMessage, releaseId }}\"}"));
	}

}
