package com.synquarium.api.domain.sync;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
@Profile("api")
public interface SyncTargetRepository extends CrudRepository<SyncTarget, Long> {

	List<SyncTarget> findBySyncId(Long syncId);
	
}
