package com.synquarium.graphql;

import java.util.Date;

import org.springframework.stereotype.Component;

import graphql.language.IntValue;
import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

@Component
public class DateScalar extends GraphQLScalarType {

	public DateScalar() {
		super("Date", "Date scalar for synquarium", new Coercing<Date, Long>() {
			@Override
			public Long serialize(Object dataFetcherResult) {
				if (!(dataFetcherResult instanceof Date)) {
					throw new CoercingSerializeException("Cannot serialize " + dataFetcherResult + " into a string.");
				}
				return ((Date) dataFetcherResult).getTime();
			}
			@Override
			public Date parseValue(Object input) {
				return new Date(Long.parseLong(input.toString()));
			}
			@Override
			public Date parseLiteral(Object input) {
				Long value = null;
				if (input instanceof StringValue) {
					value = Long.valueOf(((StringValue) input).getValue());
				} else if (input instanceof IntValue) {
					value = ((IntValue) input).getValue().longValue();
				} else {
					//TODO: This will not work, throw an exception:
					value = Long.parseLong(input.toString());
				}
				return new Date(value);
			}
		});
	}

	public DateScalar(String name, String description, Coercing<Date, String> coercing) {
		super(name, description, coercing);
	}

}
