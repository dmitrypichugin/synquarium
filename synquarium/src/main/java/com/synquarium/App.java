package com.synquarium;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.coxautodev.graphql.tools.SchemaParser;
import com.synquarium.agent.InstanceInfoWatcher;
import com.synquarium.agent.domain.instance.InstanceInfoRepository;
import com.synquarium.agent.domain.instance.InstanceInfoResolver;
import com.synquarium.agent.domain.instance.InstanceMutationAgentResolver;
import com.synquarium.agent.domain.synctrace.SyncTraceQueryAgentResolver;
import com.synquarium.agent.domain.synctrace.SyncTraceRepository;
import com.synquarium.api.domain.instance.Instance;
import com.synquarium.api.domain.instance.InstanceMutationApiResolver;
import com.synquarium.api.domain.instance.InstanceRepository;
import com.synquarium.api.domain.instance.InstanceResolver;
import com.synquarium.api.domain.release.Release;
import com.synquarium.api.domain.release.ReleaseRepository;
import com.synquarium.api.domain.release.ReleaseResolver;
import com.synquarium.api.domain.sync.Sync;
import com.synquarium.api.domain.sync.SyncMutationResolver;
import com.synquarium.api.domain.sync.SyncQueryResolver;
import com.synquarium.api.domain.sync.SyncRepository;
import com.synquarium.api.domain.sync.SyncResolver;
import com.synquarium.api.domain.sync.SyncTarget;
import com.synquarium.api.domain.sync.SyncTargetRepository;
import com.synquarium.api.domain.synctrace.SyncTraceQueryApiResolver;
import com.synquarium.api.domain.target.Target;
import com.synquarium.api.domain.target.TargetRepository;
import com.synquarium.api.domain.target.TargetResolver;
import com.synquarium.common.domain.instance.InstanceInfo;
import com.synquarium.common.domain.instance.InstanceInfo.SyncScheduleEnum;
import com.synquarium.graphql.DateScalar;
import com.synquarium.security.oauth.User;
import com.synquarium.security.oauth.UserRepository;

import graphql.schema.GraphQLSchema;
import graphql.servlet.SimpleGraphQLServlet;

@SpringBootApplication
public class App {

	public static void main(String[] args) {
//		SpringApplication.run(App.class, args);
		SpringApplication app = new SpringApplication(App.class);
		app.setAdditionalProfiles(getMode(args));
		app.run(args);
	}
	
	private static String getMode(String[] args) {
		List<String> modeOptions = new DefaultApplicationArguments(args).getOptionValues("mode");
		if (modeOptions == null || modeOptions.size() == 0) {
			System.err.println("The application mode is not specified. Supported modes: \n\t --mode=api \n\t --mode=agent");
			System.exit(-1);
		}
		String mode = modeOptions.get(0).toLowerCase();
		if (!Arrays.asList("agent", "api").contains(mode)) {
			System.err.println("Invalid application mode is not specified. . Supported modes: \\n\\t --mode=api \\n\\t --mode=agent");
			System.exit(-1);
		}
		return mode;
	}

	
	@Configuration
	@Profile("agent")
	public static class ApiApp {
		@Bean
		public ApplicationRunner demo(
				SyncTraceRepository synchRepo,
				InstanceInfoWatcher instanceInfoWatcher,
				InstanceInfoRepository instanceInfoRepo) {
			return (ApplicationArguments args) -> {
	
				InstanceInfo instanceInfo = new InstanceInfo();
				instanceInfo.setId(1L);
				instanceInfo.setReleaseId(3L);
				instanceInfo.setLastSynced(new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 100));
				instanceInfo.setSyncSchedule(SyncScheduleEnum.AUTO);
				instanceInfoRepo.save(instanceInfo);
							
				System.out.println("Starting the watcher..." + args.getOptionValues("mode"));
				
				instanceInfoWatcher.startWatching();
				
			};
		}
		@Autowired private InstanceInfoResolver instanceInfoResolver;
		@Autowired private InstanceMutationAgentResolver instanceMutationResolver;
		@Autowired private SyncTraceQueryAgentResolver syncTraceQueryResolver;
		@Autowired private DateScalar dateScalar;
		
		@Bean
		public GraphQLSchema schema(ApplicationArguments args) {
			System.out.println("MODE: " + args.getOptionValues("mode"));
			return SchemaParser.newParser()
					.files("agent.graphqls-hidden")
					.resolvers(
							syncTraceQueryResolver,
							instanceInfoResolver,
							instanceMutationResolver)
					.scalars(dateScalar)
					.build()
					.makeExecutableSchema();
		}
		
		@Bean
		ServletRegistrationBean graphQLServletRegistrationBean(ApplicationArguments args) {
			return new ServletRegistrationBean(SimpleGraphQLServlet.builder(schema(args)).build(), "/graphql");
		}
	}
	
	@Configuration
	@Profile("api")
	public static class AgentApp {
		@Bean
		public CommandLineRunner demo(
				SyncRepository synchRepo,
				TargetRepository targetRepo,
				ReleaseRepository releaseRepo,
				SyncTargetRepository syncTargetRepo,
				InstanceRepository instanceRepo,
				UserRepository userRepo) {
			return (args) -> {
				Release r1801 = releaseRepo.save(new Release("18.01"));
				Release r1802 = releaseRepo.save(new Release("18.02", r1801.getId()));
				Release r180201 = releaseRepo.save(new Release("18.02.1", r1802.getId()));
				Release r1803 = releaseRepo.save(new Release("18.03", r1802.getId()));
				Release r1804 = releaseRepo.save(new Release("18.04", r1803.getId()));
				Release r1805 = releaseRepo.save(new Release("18.05", r1804.getId()));

				instanceRepo.save(new Instance("Equity-QA",   "http://localhost:8081"));
				instanceRepo.save(new Instance("DUCA-QA",     "http://localhost:8081"));
				instanceRepo.save(new Instance("HMECU-QA",    "http://localhost:8081"));
				instanceRepo.save(new Instance("MemberCU-QA", "http://localhost:8081"));
				
				Target t1 = targetRepo.save(new Target("HMECU"));
				Target t2 = targetRepo.save(new Target("DUCA"));
				Target t3 = targetRepo.save(new Target("Equity"));
				Target t4 = targetRepo.save(new Target("MemberCU"));
				
				Sync s1 = synchRepo.save(new Sync("Script 1", "ALTER TABLE person ADD FIELD age INT;",
						555L, null, false, r1801.getId(), 1L));
				Sync s2 = synchRepo.save(new Sync("Script 2", "INSERT INTO person (id, name, age) VALUES (1, 'Vasya', 32);",
						555L, null, false, r1802.getId(), 1L));
				Sync s3 = synchRepo.save(new Sync("Script 3", "INSERT INTO person (id, name, age) VALUES (1, 'Kolya', 43);",
						555L, null, false, r1803.getId(), 1L));
				Sync s4 = synchRepo.save(new Sync("Script 4", "INSERT INTO person (id, name, age) VALUES (1, 'Kolya1', 43);",
						555L, null, false, r1803.getId(), 2L));
				Sync s5 = synchRepo.save(new Sync("Script 5", "INSERT INTO person (id, name, age) VALUES (1, 'Kolya2', 43);",
						555L, null, false, r1803.getId(), 3L));
				Sync s6 = synchRepo.save(new Sync("Script 6", "INSERT INTO person (id, name, age) VALUES (1, 'Kolya3', 43);",
						555L, null, false, r1803.getId(), 4L));

				syncTargetRepo.save(new SyncTarget(s1.getId(), t1.getId()));
				syncTargetRepo.save(new SyncTarget(s4.getId(), t1.getId()));
				syncTargetRepo.save(new SyncTarget(s5.getId(), t1.getId()));
				syncTargetRepo.save(new SyncTarget(s6.getId(), t1.getId()));
				syncTargetRepo.save(new SyncTarget(s2.getId(), t2.getId()));
				syncTargetRepo.save(new SyncTarget(s3.getId(), t2.getId()));
				syncTargetRepo.save(new SyncTarget(s2.getId(), t3.getId()));
				syncTargetRepo.save(new SyncTarget(s3.getId(), t3.getId()));
				syncTargetRepo.save(new SyncTarget(s3.getId(), t4.getId()));
				
				userRepo.save(new User("A A", "a@a.com", "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.AhLukKwq2vhWqIUhtn/mzEa1FbNNIQ6"));
				userRepo.save(new User("B B", "b@b.com", "$2a$04$PCIX2hYrve38M7eOcqAbCO9UqjYg7gfFNpKsinAxh99nms9e.8HwK"));
				userRepo.save(new User("C C", "c@c.com", "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu"));
			};
		}
		
		@Autowired private InstanceResolver instanceResolver;
		@Autowired private InstanceMutationApiResolver instanceMutationResolver;
		@Autowired private ReleaseResolver releaseResolver;
		@Autowired private SyncMutationResolver syncMutationResolver;
		@Autowired private SyncQueryResolver syncQueryResolver;
		@Autowired private SyncResolver syncResolver;
		@Autowired private SyncTraceQueryApiResolver syncTraceQueryResolver;
		@Autowired private TargetResolver targetResolver;
		@Autowired private DateScalar dateScalar;
		
		@Bean
		public GraphQLSchema schema(ApplicationArguments args) {
			System.out.println("MODE: " + args.getOptionValues("mode"));
			return SchemaParser.newParser()
					.files("api.graphqls-hidden")
					.resolvers(
							instanceResolver,
							instanceMutationResolver,
							releaseResolver,
							syncMutationResolver,
							syncQueryResolver,
							syncResolver,
							syncTraceQueryResolver,
							targetResolver)
					.scalars(dateScalar)
					.build()
					.makeExecutableSchema();
		}
		
	}
}
