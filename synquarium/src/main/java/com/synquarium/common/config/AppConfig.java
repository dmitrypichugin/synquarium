package com.synquarium.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import graphql.schema.GraphQLSchema;
import graphql.servlet.SimpleGraphQLServlet;

@Configuration
public class AppConfig {

	@Autowired private GraphQLSchema schema;
	
	@Bean
	public ServletRegistrationBean<SimpleGraphQLServlet> graphQLServletRegistrationBean(ApplicationArguments args) {
		return new ServletRegistrationBean<SimpleGraphQLServlet>(
				SimpleGraphQLServlet.builder(schema).build(), "/graphql");
	}
	
}
