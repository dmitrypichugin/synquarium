package com.synquarium.common.domain.synctrace;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import com.synquarium.agent.SyncStatusEnum;

@Entity
public class SyncTrace {
	
	@Id
	private Long id;
	private @CreationTimestamp Date created;
	private @CreationTimestamp Date lastSynced;
	private String hash; // Hash code of the body field
	private SyncStatusEnum syncStatus; //TODO: chance to syncStatusCode
	private String syncMessage; // Explains why the sync failed TODO: change to syncMessageCode
	private Long releaseId;
	
	@Transient
	private String body;
	
	public SyncTrace() {}
	
	public SyncTrace(Long id, String hash) {
		this.id = id;
		this.hash = hash;
	}

	@Override
	public String toString() {
		return "SyncTrace [id=" + id + ", created=" + created + ", lastSynced=" + lastSynced + ", hash=" + hash
				+ ", syncStatus=" + syncStatus + ", syncMessage=" + syncMessage + ", releaseId=" + releaseId + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public SyncStatusEnum getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(SyncStatusEnum syncStatus) {
		this.syncStatus = syncStatus;
	}

	public String getSyncMessage() {
		return syncMessage;
	}

	public void setSyncMessage(String syncMessage) {
		this.syncMessage = syncMessage;
	}

	public Long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}

	public Date getLastSynced() {
		return lastSynced;
	}

	public void setLastSynced(Date lastSynced) {
		this.lastSynced = lastSynced;
	}
	
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
