package com.synquarium.common.domain.instance;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InstanceInfo {

	@Id
	private Long id;
	private Long releaseId;
	private Long releaseIdToStartWith; // Syncs from earlier releases will not be applied.
	private Date lastSynced;
	private SyncScheduleEnum syncSchedule;
	private Boolean fullRestoreRequired;
	
	public static enum SyncScheduleEnum {
		MANUAL,
		AUTO,
	}

	@Override
	public String toString() {
		return "InstanceInfo [id=" + id + ", releaseId=" + releaseId + ", releaseIdToStartWith=" + releaseIdToStartWith
				+ ", lastSynced=" + lastSynced + ", syncSchedule=" + syncSchedule + ", fullRestoreRequired="
				+ fullRestoreRequired + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}

	public Date getLastSynced() {
		return lastSynced;
	}

	public void setLastSynced(Date lastSynced) {
		this.lastSynced = lastSynced;
	}

	public SyncScheduleEnum getSyncSchedule() {
		return syncSchedule;
	}

	public void setSyncSchedule(SyncScheduleEnum syncSchedule) {
		this.syncSchedule = syncSchedule;
	}

	public Boolean getFullRestoreRequired() {
		return fullRestoreRequired;
	}

	public void setFullRestoreRequired(Boolean fullRestoreRequired) {
		this.fullRestoreRequired = fullRestoreRequired;
	}

	public Long getReleaseIdToStartWith() {
		return releaseIdToStartWith;
	}

	public void setReleaseIdToStartWith(Long releaseIdToStartWith) {
		this.releaseIdToStartWith = releaseIdToStartWith;
	}
	
}
