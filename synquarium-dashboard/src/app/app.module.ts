import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql';
import { CoreModule } from './core';
import { MainModule } from './main';
import { SyncsModule } from './syncs';
import { SettingsModule } from './settings/settings.module';
import { InstancesModule } from './instances/instances.module';
import { NgModule, VERSION } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		GraphQLModule,
		CoreModule,
		MainModule,
		SyncsModule,
		InstancesModule,
		SettingsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor() {
		console.log(`Angular v${VERSION.full}`);
	}
}
