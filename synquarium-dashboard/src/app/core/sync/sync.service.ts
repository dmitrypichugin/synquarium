import { Sync } from './sync';
import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { Observable } from 'rxjs';
import { FetchResult } from 'apollo-link';

const findSyncsQuery = gql`
	query findSyncsQuery($releaseId: Long!) {
		findSyncs(releaseId: $releaseId) {
			id,
			name,
			body,
			created,
			updated,
			targets,
			releaseId,
			position,
			requiresSyncSince
		}
	}`;

const addSyncMutation = gql`
	mutation createSync($name: String!, $body: String!, $releaseId: ID!, $position: Long, $targetIds: [ID]) {
		createSync(name: $name, body: $body, releaseId: $releaseId, position: $position, targetIds: $targetIds) {
			id,
			name,
			body,
			created,
			updated,
			targets,
			releaseId,
			position,
			requiresSyncSince
		}
	}`;

const updateSyncMutation = gql`
	mutation updateSync($id: ID!, $sync: SyncInput, $targetIds: [ID]) {
		updateSync(id: $id, sync: $sync, targetIds: $targetIds) {
			id,
			name,
			body,
			created,
			updated,
			targets,
			releaseId,
			position,
			requiresSyncSince
		}
	}`;

const deleteSyncMutation = gql`
	mutation deleteSync($id: ID!) {
		deleteSync(id: $id)
	}`;

@Injectable()
export class SyncService {

	constructor(private apollo: Apollo) { }

	getSyncs(releaseId: number): Observable<ApolloQueryResult<{}>> {
		return this.apollo.query({ query: findSyncsQuery, variables: { releaseId: releaseId} });
	}

	addSync(newSync: Sync, releaseId: number): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: addSyncMutation,
			variables: {
		 		name: newSync.name,
				body: newSync.body,
				releaseId: releaseId,
				position: newSync.position,
				targetIds: newSync.targets,
			},
			update: (proxy, { data: { createSync } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery({ query: findSyncsQuery, variables: { releaseId: releaseId } });
				// Add our todo from the mutation to the end.
				data['findSyncs'].push(createSync);
				// Write our data back to the cache.
				proxy.writeQuery({ query: findSyncsQuery, variables: { releaseId: releaseId }, data });
			}
		});
	}

	updateSync(sync, releaseId: number): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: updateSyncMutation,
			variables: {
		 		id: sync.id,
				sync: { // Sync contains more fields than the updateSync can handle - so for now let's just re-create the object
					id: sync.id,
					name: sync.name,
					body: sync.body,
				},
				targetIds: sync.targets
			},
			update: (proxy, { data: { updateSync } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery({ query: findSyncsQuery, variables: { releaseId: releaseId } });
				// Add our todo from the mutation to the end.
				let updatedSyncIndex = data['findSyncs'].findIndex(s => s.id == sync.id);
				data['findSyncs'][updatedSyncIndex] = updateSync;
				// Write our data back to the cache.
				proxy.writeQuery({ query: findSyncsQuery, variables: { releaseId: releaseId }, data });
			}
		});
	}

	moveSync(sync, releaseId: number): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: updateSyncMutation,
			variables: {
		 		id: sync.id,
				sync: { // Sync contains more fields than the updateSync can handle - so for now let's just re-create the object
					id: sync.id,
					position: sync.position,
				},
				targetIds: sync.targets
			},
			update: (proxy, { data: { updateSync } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery({ query: findSyncsQuery, variables: { releaseId: releaseId } });
				// Add our todo from the mutation to the end.
				let updatedSyncIndex = data['findSyncs'].findIndex(s => s.id == sync.id);
				data['findSyncs'][updatedSyncIndex] = updateSync;
				// Write our data back to the cache.
				proxy.writeQuery({ query: findSyncsQuery, variables: { releaseId: releaseId }, data });
			}
		});
	}

	deleteSync(id: number, releaseId: number): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: deleteSyncMutation,
			variables: { id },
			update: (proxy, { data: { deleteSync } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery({ query: findSyncsQuery, variables: { releaseId: releaseId } });
				// Add our todo from the mutation to the end.
				let deletedSyncIndex = data['findSyncs'].findIndex(s => s.id == id);
				data['findSyncs'].splice(deletedSyncIndex, 1);
				// Write our data back to the cache.
				proxy.writeQuery({ query: findSyncsQuery, variables: { releaseId: releaseId }, data });
			}
		});
	}

}
