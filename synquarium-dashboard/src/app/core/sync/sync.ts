export interface Sync {
	id: number
	name: string
	body: string
	created: number
	updated: number
	authorId: number
	hash: String
	approved: boolean
	releaseId: number
	targets: Array<number>
	position: number
	requiresSyncSince: number
}
