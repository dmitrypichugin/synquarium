import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable } from 'rxjs/Observable';
import { ApolloQueryResult } from 'apollo-client';
import { map } from 'rxjs/operators';
import { Release } from './release';

type ReleasesQuery = { getAllReleases };
@Injectable()
export class ReleaseService {

	constructor(private apollo: Apollo) { }

	getReleases(): Observable<Array<Release>>  {
		return this.apollo.query<ReleasesQuery>({ query: gql`{
			getAllReleases {
				id
				name
				parentId
			}
		}`}).pipe(map(({data}) => data.getAllReleases));
	}
}
