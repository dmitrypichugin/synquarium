import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReleaseService } from './release/release.service';
import { TargetService } from './target/target.service';
import { TopBarComponent } from './top-bar/top-bar.component';

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
	],
	declarations: [
		TopBarComponent,
	],
	exports: [
		TopBarComponent,
	],
	providers: [
		ReleaseService,
		TargetService,
	]
})
export class CoreModule {
	/* make sure CoreModule is imported only by one NgModule the AppModule */
	constructor(
		@Optional() @SkipSelf() parentModule: CoreModule
	) {
		if (parentModule) {
			throw new Error('CoreModule is already loaded. Import only in AppModule');
		}
	}
}
