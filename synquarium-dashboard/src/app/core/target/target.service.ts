import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs/Observable';
import { ApolloQueryResult } from 'apollo-client';
import gql from 'graphql-tag';

@Injectable()
export class TargetService {

	constructor(private apollo: Apollo) { }

	getTargets(): Observable<ApolloQueryResult<{getAllTargets}>>  {
		return this.apollo.query({ query: gql`{
			getAllTargets {
				id
				name
				updated
				created
			}
		}`});
	}

}
