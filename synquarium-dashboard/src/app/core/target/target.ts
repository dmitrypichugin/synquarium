export interface Target {
	id: number;
	name: string;
	created: number;
	updated: number;
}
