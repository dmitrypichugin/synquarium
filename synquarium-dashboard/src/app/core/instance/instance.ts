export interface Instance {
	id: number;
	name: string;
	url: string;
	releaseId: number;
}

export interface InstanceInfo {
	id: number;
	releaseId: number;
	lastSynced: number;
	syncSchedule: string;
	fullRestoreRequired: boolean;
}

export interface SyncTrace {
	id: number;
	created: number
	lastSynced: number
	hash: string
	syncStatus: string
	syncMessage: string
	releaseId: number
}
