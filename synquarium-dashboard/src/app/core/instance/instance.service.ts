import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs/Observable';
import { ApolloQueryResult } from 'apollo-client';
import gql from 'graphql-tag';
import { Instance, InstanceInfo, SyncTrace } from './instance';
import { map } from 'rxjs/operators';
import { FetchResult } from 'apollo-link';

const getInstancesQuery = gql`
	query getInstances {
		getInstances {
			id
			name
			url
		}
	}`;

const addInstanceMutation = gql`
	mutation createInstance($instance: InstanceInput) {
		createInstance(instance: $instance) {
			id
			name
			url
		}
	}`;

const updateInstanceMutation = gql`
	mutation updateInstance($instance: InstanceInput) {
		updateInstance(instance: $instance) {
			id
			name
			url
		}
	}`;

const deleteInstanceMutation = gql`
	mutation deleteInstance($id: ID!) {
		deleteInstance(id: $id)
	}`;

const getInstanceInfoQuery = gql`
	query getInstanceInfo($instanceId: Long!) {
		getInstanceInfo(instanceId: $instanceId) {
			id
			releaseId
			lastSynced
			syncSchedule
			fullRestoreRequired
		}
	}`;
const updateInstanceInfoMutation = gql`
	mutation updateInstanceInfo($instanceInfo: InstanceInfoInput) {
		updateInstanceInfo(instanceInfo: $instanceInfo) {
			id
			releaseId
			lastSynced
			syncSchedule
			fullRestoreRequired
		}
	}`;

const getInstanceSyncsQuery = gql`
	query getInstanceSyncs($instanceId: Long!, $releaseId: Long!) {
		getInstanceSyncs(instanceId: $instanceId, releaseId: $releaseId) {
			id
			created
			lastSynced
			hash
			syncStatus
			syncMessage
			releaseId
		}
	}`;

type InstancesQuery = { getInstances };
type InstanceInfoQuery = { getInstanceInfo };
type InstanceSyncsQuery = { getInstanceSyncs };

@Injectable()
export class InstanceService {

	constructor(private apollo: Apollo) { }

	getInstances(): Observable<Instance[]> { 
		return this.apollo.query<InstancesQuery>({ query: getInstancesQuery }).pipe(map(({data}) => data.getInstances));
	}

	addInstance(newInstance: Instance): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: addInstanceMutation,
			variables: {
		 		instance: newInstance
			},
			update: (proxy, { data: { createInstance } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery<InstancesQuery>({ query: getInstancesQuery });
				// Add our todo from the mutation to the end.
				data.getInstances.push(createInstance);
				// Write our data back to the cache.
				proxy.writeQuery({ query: getInstancesQuery, data });
			}
		});
	}

	updateInstance(instance): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: updateInstanceMutation,
			variables: {
				instance: instance
			},
			update: (proxy, { data: { updateInstance } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery<InstancesQuery>({ query: getInstancesQuery });
				// Add our todo from the mutation to the end.
				let updatedInstanceIndex = data.getInstances.findIndex(s => s.id == instance.id);
				data.getInstances[updatedInstanceIndex] = updateInstance;
				// Write our data back to the cache.
				proxy.writeQuery({ query: getInstancesQuery, data });
			}
		});
	}

	deleteInstance(id: number): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: deleteInstanceMutation,
			variables: { id },
			update: (proxy, { data: { deleteInstance } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery<InstancesQuery>({ query: getInstancesQuery });
				// Add our todo from the mutation to the end.
				let deletedInstanceIndex = data.getInstances.findIndex(s => s.id == id);
				data.getInstances.splice(deletedInstanceIndex, 1);
				// Write our data back to the cache.
				proxy.writeQuery({ query: getInstancesQuery, data });
			}
		});
	}

	getInstanceInfo(instanceId: number): Observable<InstanceInfo> {
		return this.apollo.query<InstanceInfoQuery>(
			{ query: getInstanceInfoQuery, variables: { instanceId: instanceId} }).pipe(map(({data}) => data.getInstanceInfo));
	}

	updateInstanceInfo(instanceInfo: InstanceInfo): Observable<InstanceInfo>  {
		return this.apollo.mutate({
			mutation: updateInstanceInfoMutation,
			variables: {
				instanceInfo: {
					id: instanceInfo.id,
					releaseId: instanceInfo.releaseId,
					syncSchedule: instanceInfo.syncSchedule
				}
			},
			update: (proxy, { data: { updateInstanceInfo } }) => {
				// Read the data from our cache for this query.
				const data = proxy.readQuery<InstanceInfoQuery>({ query: getInstanceInfoQuery, variables: { instanceId: instanceInfo.id} });
				// Update the mutation:
				// let updatedInstanceInfoIndex = data.getInstanceInfo.findIndex(s => s.id == instanceInfo.id);
				// data.getInstanceInfo[updatedInstanceInfoIndex] = updateInstanceInfo;
				data.getInstanceInfo = updateInstanceInfo;
				// Write our data back to the cache.
				proxy.writeQuery({ query: getInstanceInfoQuery, variables: { instanceId: instanceInfo.id}, data });
			}
		}).pipe(map(({data}) => data.updateInstanceInfo));
	}

	getInstanceSyncs(instanceId: number, releaseId: number): Observable<Array<SyncTrace>> {
		return this.apollo.query<InstanceSyncsQuery>({
			query: getInstanceSyncsQuery,
			variables: { instanceId: instanceId, releaseId: releaseId }
		}).pipe(map(({ data }) => data.getInstanceSyncs));
	}

}
