import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectItem } from 'primeng/components/common/selectitem';

@Component({
	selector: 'synquarium-select',
	templateUrl: './select.component.html',
	styleUrls: ['./select.component.css'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		multi: true,
		useExisting: forwardRef(() => SelectComponent),
	}]
})
export class SelectComponent implements ControlValueAccessor {

	/* @Input() */ _items: Array<SelectItem> = [];
	@Input() placeholder: String = "Select";
	@Input() multiple: boolean;
	@Input() keyField: string; // optional, by default we select the whole object from the items array
	@Input() labelField: string; // optional, by default we display the whole object from the items array as a label
	@Input() empty: boolean = false; // if true, an empty element will be added to the list
	@Input() keyAsModel: boolean = true; // actually, in this project it should always be true
	itemKeys: Array<Object>;
	itemLabels: Array<Object>;
	selectedItem: Object;
	disabled: boolean;

	private propagateChange = (_: any) => { };

	@Input()
	set items(items: Array<any>) {
		this._items = [];
		if (items) {
			// this.itemKeys = this.keyField ? this._items.map(item => item[this.keyField]) : this._items;
			// this.itemLabels = this.labelField ? this._items.map(item => item[this.labelField]) : this._items;
			if (this.keyField && this.labelField) { 
				this._items = items.map(item => {
					return { 
						label: this.labelField ? item[this.labelField] : item,
						value: this.keyField ? item[this.keyField] : item,
						// original: item
					}
				});
			}
		}
		if (this.empty) {
			this._items = [{label: "", value: ""} , ...this._items];
		}
	}
	get items() { return this._items; }

	registerOnTouched(fn: any): void { }
	registerOnChange(fn: any): void {
		this.propagateChange = fn;
	}

	writeValue(obj: any): void {
		if (obj) {
			this.selectedItem = obj;
		}
	}

	setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}

	onSelected($event) {
		this.propagateChange($event.value);
	}

	onSelectedMultiple($event) {
		this.propagateChange($event.value);
	}

}
