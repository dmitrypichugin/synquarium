import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {ButtonModule} from 'primeng/button';
import {TabViewModule} from 'primeng/tabview';
import {OrganizationChartModule} from 'primeng/organizationchart';
import {ScrollPanelModule} from 'primeng/scrollpanel';

import { SelectComponent } from './input/select/select.component';

@NgModule({
	imports: [
		/* angular stuff */
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,

		/* PrimeNg components */
		TableModule,
		DialogModule,
		DropdownModule,
		MultiSelectModule,
		ButtonModule,
		TabViewModule,
		OrganizationChartModule,
		ScrollPanelModule,
	],
	declarations: [
		SelectComponent,
	],
	exports: [
		/* angular stuff */
		CommonModule,
		BrowserAnimationsModule,
		FormsModule,

		/* PrimeNg components */
		TableModule,
		DialogModule,
		DropdownModule,
		MultiSelectModule,
		ButtonModule,
		TabViewModule,
		OrganizationChartModule,
		ScrollPanelModule,

		/* our own custom components */
		SelectComponent,
	]
})
export class SharedModule { }
