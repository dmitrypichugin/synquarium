import { Component, OnInit, ViewChild } from '@angular/core';
import { Release } from '../core/release/release';
import { ReleaseService } from '../core/release/release.service';
import { Sync } from '../core/sync/sync';
import { SyncService } from '../core/sync/sync.service';
import { TargetService } from '../core/target/target.service';
import { Target } from '../core/target/target';
import { Instance, SyncTrace } from '../core/instance/instance';
import { InstanceService } from '../core/instance/instance.service';

@Component({
	selector: 'synquarium-syncs',
	templateUrl: './syncs.component.html',
	styleUrls: ['./syncs.component.css']
})
export class SyncsComponent implements OnInit {

	syncs: Sync[];
	syncsNonFiltered: Sync[];
	targets: Target[];
	releases: Release[];
	instances: Instance[];
	selectedRelease: number;
	selectedTarget: number;
	selectedInstance: Instance;
	syncTraceMap: Map<number, SyncTrace> = new Map();

	constructor(
		private syncService: SyncService,
		private releaseService: ReleaseService,
		private targetService: TargetService,
		private instanceService: InstanceService,
	) {}
	
	ngOnInit() {
		this.releaseService.getReleases().subscribe(releases => {
			this.releases = releases;
			if (this.releases && this.releases.length > 0) {
				this.setRelease(this.releases[0].id);
			}
		});
		this.targetService.getTargets().subscribe(({data: {getAllTargets}}) => this.targets = getAllTargets);
		this.instanceService.getInstances().subscribe(instances => this.instances = instances);
	}

	setRelease(release: number) {
		this.selectedRelease = release;
		this.loadSyncs();
	}

	filterByTarget(targetId: number) {
		if (targetId !== undefined) {
			this.syncs = this.syncsNonFiltered.filter(sync => !sync.targets || (sync.targets.length <= 0) || sync.targets.indexOf(targetId) >= 0 );
		} else {
			this.syncs = this.syncsNonFiltered;
		}
	}

	loadSyncs() {
		this.getSyncs();
	}
	
	getSyncs() {
		this.syncService.getSyncs(this.selectedRelease).subscribe(syncsApolloResult => {
			this.syncs = [];
			syncsApolloResult.data["findSyncs"].forEach(sync => {
				this.syncs.push(Object.assign({}, sync)); //Apollo's objects are immutable so we have to copy them in order to be able to modify them
			});
			this.syncsNonFiltered = this.syncs;
		});
	}

	// openSyncEditor(sync?: Sync) {
	// 	this.editedSync = sync || <Sync> {};
	// 	this.displaySyncEditDialog = true;
	// }

	saveSync(sync: Sync) {
		// if (!sync.id) {
		// 	sync.position = this.syncs.length + 1;
		// }
		// console.log(sync);
		// let saveObservable = sync.id
		// 	? this.syncService.updateSync(sync, this.selectedRelease.id)
		// 	: this.syncService.addSync(sync, this.selectedRelease.id)
		// saveObservable.subscribe(result => {
		// 	// this.displaySyncEditDialog = false;
		// 	sync = <Sync> {}; //new Sync();
			this.loadSyncs();
		// });
	}


	deleteSync(id: number) {
		this.syncService.deleteSync(id, this.selectedRelease).subscribe(result => this.loadSyncs());
	}

	selectInstance(instance) {
		if (!instance) {
			this.syncTraceMap = new Map();
		} else {
			this.instanceService.getInstanceSyncs(instance, this.selectedRelease).subscribe(syncTraces => {
				console.log(syncTraces);
				if (syncTraces) {
					syncTraces.forEach(syncTrace => this.syncTraceMap.set(syncTrace.id, syncTrace));
				}
			});
		}
	}

	moveRow($event) {
		console.log($event);
		if ($event.dragIndex === $event.dropIndex || $event.dragIndex + 1 === $event.dropIndex) {
			return;
		}
		if ($event.dropIndex === 0) {
			this.syncs[$event.dropIndex].position = this.syncs[$event.dropIndex + 1].position - 1;
			this.syncService.moveSync(this.syncs[$event.dropIndex], this.selectedRelease).subscribe(result => this.loadSyncs());
		} else if ($event.dragIndex > $event.dropIndex) {
			this.syncs[$event.dropIndex].position = this.syncs[$event.dropIndex - 1].position + 1;
			this.syncService.moveSync(this.syncs[$event.dropIndex], this.selectedRelease).subscribe(result => this.loadSyncs());
		} else {
			this.syncs[$event.dropIndex - 1].position = this.syncs[$event.dropIndex - 2].position + 1;
			this.syncService.moveSync(this.syncs[$event.dropIndex - 1], this.selectedRelease).subscribe(result => this.loadSyncs());
		}
	}

}
