import { Injectable } from '@angular/core';
import { Release } from '../../core/release/release';
import { TreeNode } from 'primeng/api';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs';
import { FetchResult } from 'apollo-link';

const mergeSyncsMutation = gql`
	mutation mergeSyncs($fromReleaseId: ID!, $toReleaseId: ID!) {
		mergeSyncs(fromReleaseId: $fromReleaseId, toReleaseId: $toReleaseId) {
			id,
			name,
			body,
			created,
			updated,
			targets,
			releaseId,
			position,
			requiresSyncSince
		}
	}`;

@Injectable({
	providedIn: 'root'
})
export class MergeService {

	constructor(private apollo: Apollo) { }

	public toTree(releases: Array<Release>, parentId?: number): Array<TreeNode> {
		return this.toTreeRecursive((releases || []).slice());
	}

	private toTreeRecursive(releases: Array<Release>, parentId?: number): Array<TreeNode> {
		let tree = [];
		parentId = parentId || null;
		if (releases && releases.length > 0) {
			for (let i = 0; i < releases.length; i++) {
				let release = releases[i];
				if (release.parentId === parentId) {
					releases.splice(i--, 1);
					tree.push({
						data: release.id,
						label: release.name,
						children: this.toTreeRecursive(releases, release.id),
						expanded: true,
					});
				}
			}
		}
		return tree;
	}

	public merge(fromReleaseId: number, toReleaseId: number): Observable<FetchResult<{}, Record<string, any>>> {
		return this.apollo.mutate({
			mutation: mergeSyncsMutation,
			variables: { fromReleaseId, toReleaseId },
			update: (proxy, { data: { mergeSyncs } }) => {
				// Read the data from our cache for this query.
				// const data = proxy.readQuery<InstancesQuery>({ query: getInstancesQuery });
				// // Add our todo from the mutation to the end.
				// let deletedInstanceIndex = data.getInstances.findIndex(s => s.id == id);
				// data.getInstances.splice(deletedInstanceIndex, 1);
				// // Write our data back to the cache.
				// proxy.writeQuery({ query: getInstancesQuery, data });
			}
		});
	}

}

// export interface TreeNode {
// 	label?: string;
// 	data?: any;
// 	icon?: any;
// 	expandedIcon?: any;
// 	collapsedIcon?: any;
// 	children?: TreeNode[];
// 	leaf?: boolean;
// 	expanded?: boolean;
// 	type?: string;
// 	parent?: TreeNode;
// 	partialSelected?: boolean;
// 	styleClass?: string;
// 	draggable?: boolean;
// 	droppable?: boolean;
// 	selectable?: boolean;
// }