import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { ReleaseService } from '../../core/release/release.service';
import { MergeService } from './merge.service';

@Component({
	selector: 'synquarium-merge',
	templateUrl: './merge.component.html',
	styleUrls: ['./merge.component.css']
})
export class MergeComponent implements OnInit {

	displayDialog: boolean;
	treeData: TreeNode[];
	targetReleaseId: number;
	selectedRelease: any;

	constructor(
		private releaseService: ReleaseService,
		private mergeService: MergeService) { }

	ngOnInit() {
	}

	open(releaseId: number) {
		this.targetReleaseId = releaseId;
		this.treeData = [];
		this.releaseService.getReleases().subscribe(releases => this.treeData = this.mergeService.toTree(releases));
		this.displayDialog = true;
	}

	merge() {
		if (!this.selectedRelease) {
			console.log("Release is not selected");
			return;
		}
		console.log(this.selectedRelease);
		this.mergeService.merge(this.selectedRelease.data, this.targetReleaseId).subscribe(() => this.displayDialog = false);
	}

}
