import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Sync } from '../../core/sync/sync';
import { SyncService } from '../../core/sync/sync.service';
import { Release } from '../../core/release/release';
import { TargetService } from '../../core/target/target.service';
import { Target } from '../../core/target/target';

@Component({
	selector: 'synquarium-sync-editor',
	templateUrl: './sync-editor.component.html',
	styleUrls: ['./sync-editor.component.css']
})
export class SyncEditorComponent implements OnInit {

	displaySyncEditDialog: boolean;
	editedSync: Sync = <Sync> {}; //new Sync();
	targets: Target[];
	selectedRelease: number;
	
	@Output() onSave: EventEmitter<Sync> = new EventEmitter<Sync>();

	constructor(
		private syncService: SyncService,
		private targetService: TargetService,
	) { }

	ngOnInit() {
		this.targetService.getTargets().subscribe(({data: {getAllTargets}}) => this.targets = [...getAllTargets]);
	}

	open(release: number, sync?: Sync) {
		this.editedSync = sync || <Sync> {targets: []};
		this.displaySyncEditDialog = true;
		this.selectedRelease = release;
	}

	save() {
		if (!this.editedSync.id) {
			// this.editedSync.position = this.syncs.length + 1;                 //TODO: figure out the position on the back-end
		}
		let saveObservable = this.editedSync.id
			? this.syncService.updateSync(this.editedSync, this.selectedRelease)
			: this.syncService.addSync(this.editedSync, this.selectedRelease)
		saveObservable.subscribe(result => {
			this.displaySyncEditDialog = false;
			this.editedSync = <Sync> {}; //new Sync();
			this.onSave.emit(result.data.updateSync);
		});
	}

}
