import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncEditorComponent } from './sync-editor.component';

describe('SyncEditorComponent', () => {
  let component: SyncEditorComponent;
  let fixture: ComponentFixture<SyncEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyncEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
