import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SyncsComponent } from './syncs.component';
import { SharedModule } from '../shared';
import { SyncService } from '../core/sync/sync.service';
import { SyncEditorComponent } from './sync-editor/sync-editor.component';
import { MergeComponent } from './merge/merge.component';

@NgModule({
	imports: [
		CommonModule,
		SharedModule
	],
	declarations: [
		SyncsComponent,
		SyncEditorComponent,
		MergeComponent,
	],
	providers: [
		SyncService,
	]
})
export class SyncsModule { }
