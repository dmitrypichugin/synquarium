import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SyncsComponent } from './syncs';
import { InstancesComponent } from './instances/instances.component';
import { SettingsComponent } from './settings/settings.component';


const appRoutes: Routes = [
	{ path: 'syncs', component: SyncsComponent },
	{ path: 'instances', component: InstancesComponent },
	{ path: 'settings', component: SettingsComponent },
	{ path: '', redirectTo: '/syncs', pathMatch: 'full' },
	{ path: '**', component: SyncsComponent /* PageNotFoundComponent */ }
];

@NgModule({
	imports: [
		RouterModule.forRoot(
			appRoutes,
			// { enableTracing: true } // <-- debugging purposes only
		)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }