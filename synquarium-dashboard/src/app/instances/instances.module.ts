import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstancesComponent } from './instances.component';
import { SharedModule } from '../shared';
import { InstanceService } from '../core/instance/instance.service';
import { InstanceInfoComponent } from './instance-info/instance-info.component';
import { InstanceInfoEditorComponent } from './instance-info/instance-info-editor/instance-info-editor.component';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
	],
	declarations: [
		InstancesComponent,
		InstanceInfoComponent,
		InstanceInfoEditorComponent
	],
	providers: [
		InstanceService,
	]
})
export class InstancesModule { }
