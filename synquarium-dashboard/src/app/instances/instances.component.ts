import { Component, OnInit } from '@angular/core';
import { InstanceService } from '../core/instance/instance.service';
import { Instance } from '../core/instance/instance';
import { ReleaseService } from '../core/release/release.service';
import { Release } from '../core/release/release';
import { SelectItem } from 'primeng/components/common/selectitem';

@Component({
	selector: 'synquarium-instances',
	templateUrl: './instances.component.html',
	styleUrls: ['./instances.component.css']
})
export class InstancesComponent implements OnInit {

	releases: Array<SelectItem>;
	instances: Array<Instance>;
	selectedInstance: Instance;
	newInstance: boolean;
	instance: Instance;
	displayDialog: boolean;

	constructor(
		private instanceService: InstanceService,
		private releaseService: ReleaseService,
	) { }

	ngOnInit() {
		this.loadInstances();
		this.releaseService.getReleases().subscribe(releases => {
			this.releases = releases.map(r => { return {label: r.name, value: r.id}});
		});
	}
	
	private loadInstances() {
		this.instanceService.getInstances().subscribe(instances => this.instances = instances);
	}

	onInstanceSelect(instance) {
		this.newInstance = false;
		const { __typename, ...filteredInstance } = Object.assign({}, instance); // Convert to mutable object and get rid of the __typename property
		this.instance = filteredInstance;
		this.displayDialog = true;
	}

	showDialogToAdd() {
		this.newInstance = true;
		this.instance = <Instance> {};
		this.displayDialog = true;
	}

	save() {
		if (this.newInstance) {
			this.instanceService.addInstance(this.instance).subscribe(() => this.loadInstances());
		} else {
			this.instanceService.updateInstance(this.instance).subscribe(() => this.loadInstances());
		}
		this.instance = null;
		this.displayDialog = false;
	}

	delete() {
		this.instanceService.deleteInstance(this.selectedInstance.id).subscribe(() => this.loadInstances());
		this.instance = null;
		this.displayDialog = false;
	}

}
