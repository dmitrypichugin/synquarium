import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanceInfoEditorComponent } from './instance-info-editor.component';

describe('InstanceInfoEditorComponent', () => {
	let component: InstanceInfoEditorComponent;
	let fixture: ComponentFixture<InstanceInfoEditorComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [InstanceInfoEditorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(InstanceInfoEditorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
