import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { InstanceService } from '../../../core/instance/instance.service';
import { ReleaseService } from '../../../core/release/release.service';
import { InstanceInfo } from '../../../core/instance/instance';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Dialog } from 'primeng/dialog';

@Component({
	selector: 'synquarium-instance-info-editor',
	templateUrl: './instance-info-editor.component.html',
	styleUrls: ['./instance-info-editor.component.css']
})
export class InstanceInfoEditorComponent implements OnInit {

	releases: Array<SelectItem>;
	syncSchedules: Array<SelectItem> = [{label: "AUTO", value: "AUTO"}, {label: "MANUAL", value: "MANUAL"}]; //TODO: retrieve this list from the server
	instanceInfo: InstanceInfo;
	displayDialog: boolean;
	@ViewChild("instanceInfoEditor") instanceInfoEditor: Dialog;
	@Output() onSaved = new EventEmitter<InstanceInfo>();

	constructor(
		private instanceService: InstanceService,
		private releaseService: ReleaseService) { }

	ngOnInit() {
		this.releaseService.getReleases().subscribe(releases => {
			this.releases = releases.map(r => { return {label: r.name, value: r.id}});
		});
	}

	open(instanceInfo: InstanceInfo) {
		this.instanceInfo = Object.assign({}, instanceInfo);
		this.displayDialog = true;
	}

	save() {
		this.instanceService.updateInstanceInfo(this.instanceInfo).subscribe(instanceInfo => {
			this.onSaved.emit(instanceInfo);
			this.close();
		});
	}

	close() {
		this.displayDialog = false;
	}

}
