import { Component, OnInit, Input } from '@angular/core';
import { InstanceService } from '../../core/instance/instance.service';
import { InstanceInfo } from '../../core/instance/instance';
import { Release } from '../../core/release/release';
import { ReleaseService } from '../../core/release/release.service';
import { SelectItem } from 'primeng/components/common/selectitem';

@Component({
	selector: 'synquarium-instance-info',
	templateUrl: './instance-info.component.html',
	styleUrls: ['./instance-info.component.css']
})
export class InstanceInfoComponent implements OnInit {

	@Input() instanceId: number;
	releaseMap: Map<number, string>;
	releases: Array<SelectItem>;
	instanceInfo: InstanceInfo;

	constructor(
		private instanceService: InstanceService,
		private releaseService: ReleaseService) { }

	ngOnInit() {
		this.loadInstanceInfo();
		this.releaseService.getReleases().subscribe(releases => {
			this.releases = releases.map(r => { return {label: r.name, value: r.id}});
			this.releaseMap = new Map();
			releases.forEach(r => this.releaseMap.set(parseInt("" + r.id), r.name)); //TODO: Change the getAllReleases server-side method to return id as number
		});
	}

	loadInstanceInfo() {
		//TODO: The following uses the cache, make sure we actually reload data from the server:
		this.instanceService.getInstanceInfo(this.instanceId).subscribe(instanceInfo => this.instanceInfo = instanceInfo);
	}

	refresh(instanceInfo) {
		this.instanceInfo = instanceInfo
	}

}
