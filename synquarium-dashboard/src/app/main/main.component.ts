import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'synquarium-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

	// navLinks: Object = [
	// 	{ path: "/syncs", label: "Syncs" },
	// 	{ path: "/instances", label: "Instances" },
	// 	{ path: "/settings", label: "Settings" },
	// ];

	constructor() { }

	ngOnInit() {
	}

}
