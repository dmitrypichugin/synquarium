import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { SharedModule } from '../shared';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		AppRoutingModule,
	],
	declarations: [
		MainComponent
	],
	exports: [
		MainComponent
	]
})
export class MainModule { }
